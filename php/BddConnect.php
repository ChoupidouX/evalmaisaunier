<?php


class BddConnect
{
	private $NameBdd = 'testEval';
	private $IdUser = 'root';
	private $PassUser = 'ledefunt';
	private $db;

	public function __construct()
	{
		$this->BDDConnect();
	}

	public function getConnect()
	{
		return $this->db;
	}

	private function BDDConnect()
	{
		try
		{
			$this->db = new PDO('mysql:host=localhost;dbname='.$this->NameBdd.';charset=utf8', $this->IdUser , $this->PassUser);
		}
		catch (PDOException $e)
		{
			echo 'Connexion échouée : ' . $e->getMessage();
		}
	}
}