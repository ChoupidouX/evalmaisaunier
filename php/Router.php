<?php

class Router{


    private $_route;

    public function checkWay()
    {
    $mc = new Controller();
        if (isset($_GET["route"]))
        {
            $this->_route = $_GET["route"];

        if($this->_route == 'backOf')
        {
            $mc->showBackOf();
        }
        elseif($this->_route == 'home')
        {
            $mc->showHome();
        }
        elseif ($this->_route == 'connect') {
            $mc->showConnect();
        }
        elseif ($this->_route == 'subscribe') {
            $mc->showSubscribe();
        }
        elseif ($this->_route == 'logout') {
            $mc->deconnect();
        }
        
        else
        {
            $mc->show404();
        }
        }
        else
        {
            $mc->showHome();
        }
    }
}
