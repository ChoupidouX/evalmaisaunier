<?php

class Controller{


    public function showHome(){
        require("views/Home.php");
  }

    public function showBackOf(){
    if(isset($_SESSION['pseudo'])){
        require("views/BackOf.php");
    }else{
        header("Location:index.php?route=connect");
    }
      
  }

    public function show404(){
        require("views/404.php");
  }


    public function showConnect(){
        require("views/Connect.php");
  }
    
    public function showSubscribe(){
        require("views/Subscribe.php");
    }
    
    public function deconnect(){

        require("php/LogOut.php");
    }
    


 


}
