<!DOCTYPE html>
<html lang="fr">

<head>
    <title>404</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="shortcut icon" type="image/png" href="img/" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />

</head>

<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Glucotopia</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <?php
    if(isset($_SESSION['pseudo'])){
        echo "<li class='nav-item'>
        <a class='nav-link' href='?route=backOf'>Mon compte!</a>
        </li>
        <li class='nav-item'>
        <a class='nav-link' href='?route=logout'>Déconnexion</a>
        </li>";
    }else{
        echo "<li class='nav-item'><a class='nav-link' href='?route=home' >Accueil!</a></li><li class='nav-item'><a class='nav-link' href='?route=connect'>Connexion!</a></li>";   }
?>
                </ul>
            </div>
        </nav>
    </header>
    <section class="main container-fluid">
        <div class="row justify-content-center">
            <div class="col-6 col-sm-4 col-md-4 col-lg-4 col-xl-4 text-center 404 box formbox">
                <h1>404</h1>
                <p>Revenez à l'accueil</p>
                <?php
    if(isset($_SESSION['pseudo'])){
        echo "<a class='btn btn-primary' href='?route=backOf'>Mon compte!</a>
        <a class='nav-link' href='?route=logout'>Déconnexion</a>";
    }else{
        echo "
        <a class='btn btn-primary'  href='?route=home' >Accueil!</a>"; }
?>
            </div>
        </div>
    </section>


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
