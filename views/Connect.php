<?php
require_once('php/Loging.php') ?>

<!DOCTYPE html>
<html lang="fr">

<head>

    <title>Connexion</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="shortcut icon" type="image/png" href="img/" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />

</head>

<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Glucotopia</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="?route=home">Acceuil<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?route=subscribe">S'inscrire!</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <section class="main container-fluid">
        <div class="row justify-content-center">
            <div class="formbox box col-7 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center">
                <h1>Veuillez vous connecter à votre compte!</h1>
                <form action=" " method="post" class="form-group">
                    <label for="pseudo">Utilisateur: *</label>
                    <input type="text" name="pseudo" id="pseudo" class="form-control" placeholder="patate"/>
                    <label for="password"> Password: *</label>
                    <input type="password" name="password" id="password" class="form-control" placeholder="patate"/>
                    <button type="submit" name="submit"  class="btn btn-primary btnLog" >Connexion!</button>
                </form>
                <?php
				 if(isset($errorSend))
				 {
					 echo $errorSend;
				 }
				echo $errorSend; 
                ?>

                <p class="inputFormLog">* Champs obligatoires</p>
            </div>
        </div>
    </section>


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
