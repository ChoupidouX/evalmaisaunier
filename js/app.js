let foodMenus = document.getElementById("Menus");
let foodModal = document.getElementById("listAddModal");
let foodTotal = document.getElementById("cgTotal");

let myRequest = new Request("datas/ig.json");

Element.prototype.remove = function () {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function () {
    for (var i = this.length - 1; i >= 0; i--) {
        if (this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}

//read,set and get JSON!
fetch(myRequest)
    .then(function (response) {
        return response.json();
    })
    .then(function (data) {
        data.forEach(function (menus) {
            let totalTaux = menus.taux;
            let listFood = document.createElement("div");
            listFood.setAttribute("style", 'background: url(img/' + menus.img + ') no-repeat center ; background-size: cover; ');
            listFood.classList.add("col-5", "col-sm-4", "col-md-4", "col-lg-2", "col-xl-2", "text-center", "OrganisationOffood");
            listFood.innerHTML = '<span id="' + menus.aliment + '" class="foodDesc">' + menus.aliment + '</span><br/><span class="foodDesc">IG/100g :  </span><span class="foodDesc">' + menus.taux + '</span><br/><div class="upDownResult" ><button id="btnDown" type="button" class="btn choiseBtn col-2 btn-light ' + menus.aliment + '" onclick="droopDown(' + menus.aliment + ',' + menus.taux + ',' + totalTaux + ')"><i class="fa fa-minus-circle posRelatIframeDown"></i></button><span class="foodDesc qty col-1">' + menus.taux + '</span><button id="btnUp" type="button" class="btn choiseBtn col-2 btn-light ' + menus.aliment + '"  onclick="addInModal(' + menus.aliment + ',' + menus.taux + ')"><i class="fa fa-plus-circle posRelatIframeUp"></i></button></div>';

            foodMenus.appendChild(listFood);


        });
    });




//function add div and info of food
function addInModal(idAliment, idTaux) {
    let Aliment = idAliment.textContent;
    let Taux = idTaux;
    let foodModalLength = foodModal.getElementsByClassName(Aliment);
    let listForCount = document.getElementsByClassName(Aliment);
    console.log(listForCount);
    let lengthModal = foodModalLength.length;
    let lengthTabFood = listForCount.length;
    let totalTaux = '';
    if (lengthModal != lengthTabFood) {
        let totalTaux = Taux;
        let alimodal = document.createElement("div");
        alimodal.id = Aliment;
        alimodal.classList.add("col-11", "col-sm-11", "col-md-11", "col-lg-11", "col-xl-11", "text-center", "OrganisationOffood");
        alimodal.innerHTML = '<span id="' + listForCount + '" class="foodDesc">' + Aliment + '</span><br/><span class="foodDesc">IG/100g :  </span><span class="foodDesc">' + Aliment + '</span><br/><div class="upDownResult" ><button id="' + Aliment + '" type="button" class="btn choiseBtn col-2 btn-light ' + Aliment + '"  onclick="droopDown(' + Aliment + ',' + Taux + ',' + totalTaux + ')"><i class="fa fa-minus-circle posRelatIframeDown"></i></button><span class="foodDesc qty col-1">' + totalTaux + '</span><button id="' + Aliment + '" type="button" class="btn choiseBtn col-2 btn-light ' + Aliment + '"  onclick="addInModal(' + listForCount + ',' + totalTaux + ')" ><i class="fa fa-plus-circle posRelatIframeUp"></i></button></div>';
        foodModal.appendChild(alimodal);
    } else {
        totalTaux += Taux;
    };



    console.log(Aliment, Taux, lengthTabFood, lengthModal, totalTaux);

}


//function of down quantity of food select
function droopDown(idAliment, idTaux , idTotal) {
    let Aliment = idAliment;
    let Taux = idTaux;
    let destroy = document.getElementsByClassName(Aliment);
    let totalTaux = idTotal;

    if (Taux < totalTaux) {
        totalTaux -= Taux;
    } else if (Taux == totalTaux){
        destroy.remove();
    };

}


//function calcul of Charge glycémique
function totalCg() {

    let totaltaux = document.querySelectorAll(".qty").textContent;
    let total = totaltaux * totaltaux / 100;
    foodTotal.innerHTML = total;

}
