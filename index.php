<?php
session_start();
// Affiche les erreurs 
ini_set('display_errors', 1);


//auto load PDO
function chargerClasse($classe){
    require 'php/'. $classe . '.php';
}
spl_autoload_register('chargerClasse');

//log a la bdd
$bdd = new BddConnect;
$request = $bdd->getConnect();
$route = new Router();
$route->checkWay();
